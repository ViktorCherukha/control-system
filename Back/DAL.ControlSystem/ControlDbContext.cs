﻿using DAL.ControlSystem.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Text;
using System.Security.Cryptography;

namespace DAL.ControlSystem
{
    public class ControlDbContext : IdentityDbContext<User,AppRole,int,IdentityUserClaim<int>,
        AppUserRole,IdentityUserLogin<int>,IdentityRoleClaim<int>,IdentityUserToken<int>>

    {


        public ControlDbContext()
        {
           
        }

        public ControlDbContext(DbContextOptions<ControlDbContext> options) : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source = (LocalDb)\MSSQLLocalDB; Initial Catalog = ControlDb; Integrated Security = True");
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ProjectUserPosition>()
                .HasKey(t => new
                {
                    t.ProjectId,
                    t.PositionId,
                    t.UserId
                });

          

            builder.Entity<User>()
                .HasMany(ur => ur.UserRoles)
                .WithOne(u => u.User)
                .HasForeignKey(ur => ur.UserId)
                .IsRequired();

            builder.Entity<AppRole>()
                .HasMany(ur => ur.UserRoles)
                .WithOne(u => u.Role)
                .HasForeignKey(ur => ur.RoleId)
                .IsRequired();


            builder.Entity<JobStatus>(entity =>
            {
         
                entity.HasOne(d => d.Status)
                    .WithMany()
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_JobStatus_To_JobStatusType");

                entity.HasOne(e => e.Job)
                      .WithMany()
                      .HasForeignKey(e => e.JobId)
                      .HasConstraintName("FK_jobstatus_to_job");

                entity.HasOne(d => d.User)
                    .WithMany()
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_JobStatus_To_User");
            });


            //builder.Entity<ProjectState>().HasData(
            //     new ProjectState
            //     {
            //         Id = 1,
            //         State = "Open"
            //     },
            //     new ProjectState
            //     {
            //         Id = 2,
            //         State = "Close"
            //     });

            //builder.Entity<JobStatusType>().HasData(
            //    new JobStatusType
            //    {
            //        Id = 1,
            //        Status = "In process"
            //    },

            //    new JobStatusType
            //    {
            //        Id = 2,
            //        Status = "Waiting to approve"
            //    },

            //    new JobStatusType
            //    {
            //        Id = 3,
            //        Status = "Completed"
            //    });

            //builder.Entity<UserPosition>().HasData(
            //    new UserPosition
            //    {
            //        Id = 1,
            //        Position = "junior",
            //    },
            //    new UserPosition
            //    {
            //        Id = 2,
            //        Position = "middle"
            //    },
            //    new UserPosition
            //    {
            //        Id = 3,
            //        Position = "senior"
            //    },
            //    new UserPosition
            //    {
            //        Id = 4,
            //        Position = "Lead"
            //    }
            //    );

            //builder.Entity<Project>().HasData(
            //new Project
            //{
            //    Id = 1,
            //    CloseDate = DateTime.Now.AddDays(30),
            //    Name = "Car factory",
            //    StartDate = DateTime.Now.AddDays(-5),
            //    StateId = 1,

            //},

            //new Project
            //{
            //    Id = 2,
            //    CloseDate = DateTime.Now.AddDays(10),
            //    Name = "Financial company",
            //    StartDate = DateTime.Now.AddDays(-2),
            //    StateId = 1,

            //},

            //new Project
            //{
            //    Id = 3,
            //    CloseDate = DateTime.Now.AddDays(-1),
            //    Name = "Sweet company",
            //    StartDate = DateTime.Now.AddDays(-12),
            //    StateId = 2,

            //});



            //builder.Entity<Job>().HasData(
            //    new Job
            //    {
            //        Id = 1,
            //        DeadLine = DateTime.Now.AddDays(15),
            //        Start = DateTime.Now,
            //        ProjectId = 1,

            //        JobProgress = 5,

            //    },
            //    new Job
            //    {
            //        Id = 2,
            //        DeadLine = DateTime.Now.AddDays(10),
            //        Start = DateTime.Now,
            //        ProjectId = 2,

            //        JobProgress = 15,
            //    },
            //    new Job
            //    {
            //        Id = 3,
            //        DeadLine = DateTime.Now.AddDays(10),
            //        Start = DateTime.Now,
            //        ProjectId = 2,

            //        JobProgress = 25,
            //    }
            //    );



            //builder.Entity<User>().HasData(
            //    new User
            //    {
            //        Id = 1,
            //        City = "Kyiv",
            //        UserName = "Ivan Dalnov",
            //        NormalizedUserName = "Ivan Dalnov",
            //        DateOfBirth = DateTime.Now.AddDays(-9000),
            //        Gender = "male",
            //        Email = "test@test.ua",
            //        NormalizedEmail = "test@test.ua"
            //    },
            //    new User
            //    {
            //        Id = 2,
            //        City = "Kyiv",
            //        UserName = "Mukola Topolya",
            //        NormalizedUserName = "Mukola topolya",
            //        NormalizedEmail = "tester@gmail.ua",
            //        DateOfBirth = DateTime.Now.AddDays(-9000),
            //        Gender = "male",
            //        Email = "tester@gmail.ua"

            //    },
            //    new User
            //    {
            //        Id = 3,
            //        City = "Kyiv",
            //        UserName = "Admin Admin",
            //        NormalizedUserName = "Admin Admin",
            //        DateOfBirth = DateTime.Now.AddDays(-9000),
            //        Gender = "male",
            //        Email = "gonchar@gmail.ua",
            //        NormalizedEmail = "gonchar@gmail.ua",

            //    });

            //builder.Entity<ProjectUserPosition>()
            //    .HasData
            //    (
            //     new ProjectUserPosition
            //     {
            //         UserId = 1,
            //         PositionId = 1,
            //         ProjectId = 1
            //     },
            //     new ProjectUserPosition
            //     {
            //         UserId = 2,
            //         PositionId = 2,
            //         ProjectId = 2
            //     },
            //     new ProjectUserPosition
            //     {
            //         UserId = 2,
            //         PositionId = 1,

            //         ProjectId = 2
            //     }
            //    );


            //builder.Entity<JobStatus>().HasData(

            //    new
            //    {
            //        Id = 1,
            //        JobId = 1,
            //        StatusId = 1,
            //        UserId = 1,
            //        Description = "Fix some icon",
            //        LastUpdate = DateTime.Now
            //    },
            //    new
            //    {
            //        Id = 2,
            //        JobId = 2,
            //        StatusId = 1,
            //        UserId = 2,
            //        Description = "Fix some button",
            //        LastUpdate = DateTime.Now
            //    },
            //    new
            //    {
            //        Id = 3,
            //        JobId = 3,
            //        StatusId = 1,
            //        UserId = 1,
            //        Description = "Fix some label",
            //        LastUpdate = DateTime.Now
            //    }
            //    );
        }

        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectState> ProjectStates { get; set; }
        public virtual DbSet<ProjectUserPosition> ProjectUserPositions { get; set; }
        public virtual DbSet<Job> Jobs { get; set; }
        public virtual DbSet<JobStatus> JobStatuses { get; set; }
        public virtual DbSet<JobStatusType> JobStatusType { get; set; }
        public virtual DbSet<UserPosition> UserPositions { get; set; }
    }
}

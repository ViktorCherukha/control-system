﻿using DAL.ControlSystem.Entities;


namespace DAL.ControlSystem.Interfaces
{
    public interface IJobStatusRepository:IRepository<JobStatus>
    {
    }
}

﻿using DAL.ControlSystem.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.ControlSystem.Interfaces
{
    public interface IJobRepository:IRepository<Job>
    {
        Task<IEnumerable<Job>> GetByUserEmailWithDetails(string email);
        IQueryable<Job> FindAllWithDetails();
    }
}

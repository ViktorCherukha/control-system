﻿using DAL.ControlSystem.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.ControlSystem.Interfaces
{
    public interface IProjectRepository:IRepository<Project>
    {
        IQueryable<Project> FindAllWithDetails();
        Task<Project> GetByIdWithDetails(int id);
    }
}

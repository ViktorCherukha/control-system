﻿using DAL.ControlSystem.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.ControlSystem.Interfaces
{
    public interface IUserRepository:IRepository<User>
    {
        IQueryable<User> FindAllwithDetails();
        Task<bool> IsUserExist(string userName);

        Task<User> GetUserbyName(string userName);

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ControlSystem.Interfaces
{
    public interface IUnitOfWork
    {
        IJobRepository JobRepository { get; }
        IProjectRepository ProjectRepository { get; }
        IUserRepository UserRepository { get; }
        Task SaveAsync();
    }
}

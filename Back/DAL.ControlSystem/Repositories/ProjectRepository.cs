﻿using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.ControlSystem.Repositories
{
    public class ProjectRepository:IProjectRepository
    {
        private readonly ControlDbContext _context;

        public ProjectRepository(ControlDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(Project entity)
        {
            await _context.Projects.AddAsync(entity);
 
        }

        public void Delete(Project entity)
        {
             _context.Projects.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
           var project =  await _context.Projects
                .SingleOrDefaultAsync(e => e.Id == id);

            if (project == null)
                return;

            _context.Projects.Remove(project);
        }

        public IQueryable<Project> FindAll()
        {
            return _context.Projects;
        }

        public IQueryable<Project> FindAllWithDetails()
        {
            return _context.Projects.Include(e => e.State);
                                    
        }

        public async Task<Project> GetByIdAsync(int id)
        {
            return await _context.Projects
                .Include(e=>e.State)
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public async Task<Project> GetByIdWithDetails(int id)
        {
            return await _context.Projects.Include(e => e.State)
                .FirstOrDefaultAsync(e => e.Id == id);
        }

        public void Update(Project entity)
        {
            var project = _context.Projects
                .FirstOrDefault(e => e.Id == entity.Id);

            if (project == null)
                return;

            project.Name = entity.Name;
            project.StartDate = entity.StartDate;
            project.State = entity.State;
            project.StateId = entity.StateId;

            _context.Entry(project).State = EntityState.Modified;
        }
    }
}

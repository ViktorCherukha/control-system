﻿using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;


namespace DAL.ControlSystem.Repositories
{
    public class JobStatusRepository : IJobStatusRepository
    {
        private readonly ControlDbContext _context;
        public JobStatusRepository(ControlDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(JobStatus entity)
        {
            await _context.JobStatuses.AddAsync(entity);
        }

        public void Delete(JobStatus entity)
        {
            _context.JobStatuses.Remove(entity);
        }

        public async Task DeleteById(int id)
        {
            var jobStatus = _context.JobStatuses.FirstOrDefaultAsync(e => e.Id == id);
            if (jobStatus == null)
                return;
            _context.Entry(jobStatus).State = EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public IQueryable<JobStatus> FindAll()
        {
            return _context.JobStatuses
                .Include(e => e.Job)
                .Include(e => e.Status)
                .Include(e => e.User);
        }

        public async Task<JobStatus> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(JobStatus entity)
        {
            throw new NotImplementedException();
        }
    }
}

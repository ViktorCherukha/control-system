﻿using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace DAL.ControlSystem.Repositories
{
    public class UserRepository:IUserRepository
    {
        private readonly ControlDbContext _context;

        public UserRepository(ControlDbContext context)
        {
            _context = context;
        }

        public async Task AddAsync(User entity)
        {        
            await _context.Users.AddAsync(entity);
        }

        public void Delete(User entity)
        {
            _context.Users.Remove(entity);

        }

        public async Task DeleteById(int id)
        {
            var user = _context.Users.FirstOrDefaultAsync(e => e.Id == id);
            if (user == null)
                return;
            _context.Entry(user).State = EntityState.Deleted;
            await _context.SaveChangesAsync();

        }

        public IQueryable<User> FindAll()
        {
            return _context.Users;
        }

        public IQueryable<User> FindAllwithDetails()
        {
            return _context.Users.Include(e => e.Jobs);
                              
        }

        public async Task<User> GetByIdAsync(int id)
        {
            return await _context.Users
                .Include(e=>e.Jobs)
                .FirstOrDefaultAsync(u => u.Id == id);
        }

        public async Task<User> GetUserbyName(string userName)
        {
            return await _context.Users
                .FirstOrDefaultAsync(e => e.UserName == userName);
        }

        public async Task<bool> IsUserExist(string userName)
        {
            return await _context.Users
                .AnyAsync(e => e.UserName == userName);
        }



        public void Update(User entity)
        {
            var user = _context.Users
                .FirstOrDefault(e => e.Id == entity.Id);

            if (user == null)
                return;

            user.Jobs = entity.Jobs;

            _context.Entry(user).State = EntityState.Modified;

        }
    }
}

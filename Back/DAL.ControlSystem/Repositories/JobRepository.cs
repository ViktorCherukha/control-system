﻿using AutoMapper;
using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.ControlSystem.Repositories
{
    public class JobRepository : IJobRepository
    {
        private readonly ControlDbContext _context;
        public JobRepository(ControlDbContext context)
        {
            _context = context;
        }
        public async Task AddAsync(Job entity)
        {
            await _context.Jobs.AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public void Delete(Job entity)
        {
            _context.Jobs.Remove(entity);          
        }

        public async Task DeleteById(int id)
        {
            var job = _context.Jobs.FirstOrDefault(t => t.Id == id);

            if (job != null)
                _context.Entry(job).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
            await _context.SaveChangesAsync();
        }

        public IQueryable<Job> FindAll()
        {
            return _context.Jobs;
        }

        public IQueryable<Job> FindAllWithDetails()
        {
            return _context.Jobs
                .Include(p => p.Project)
                .Include(u => u.User);
        }

        public async Task<Job> GetByIdAsync(int id)
        {
           return await _context.Jobs
                .Include(e=>e.Project)
                .Include(e=>e.User)
                .FirstOrDefaultAsync(j => j.Id == id);

        }

        public async Task<IEnumerable<Job>> GetByUserEmailWithDetails(string email)
        {
            return await Task.FromResult(FindAllWithDetails().Where(e => e.User.Email == email).ToList());
        }

        public void Update(Job entity)
        {
            var job = _context.Jobs.SingleOrDefault(e => e.Id == entity.Id);

            if (job == null)
                return;

            job.JobProgress = entity.JobProgress;
            job.Project = entity.Project;
            job.ProjectId = entity.ProjectId;
            job.User = entity.User;
            job.UserId = entity.UserId;

            _context.Entry(job).State = EntityState.Modified;
            _context.SaveChanges();
        }

        
    }
}

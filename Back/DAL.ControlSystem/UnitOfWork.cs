﻿using DAL.ControlSystem.Interfaces;
using DAL.ControlSystem.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ControlSystem
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ControlDbContext _context;

        public UnitOfWork(ControlDbContext context)
        {
            _context = context;
        }
        public IJobRepository JobRepository => new JobRepository(_context);

        public IProjectRepository ProjectRepository => new ProjectRepository(_context);

        public IUserRepository UserRepository => new UserRepository(_context);

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

      
    }
}

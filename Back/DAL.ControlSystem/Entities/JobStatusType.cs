﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.ControlSystem.Entities
{
   
    public class JobStatusType
    {
        public  int Id { get; set; }
        public string Status { get; set; }
        
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.ControlSystem.Entities
{
   
    public class ProjectUserPosition
    {

        public int? UserId { get; set; }
        public int ProjectId { get; set; }
        public int PositionId { get; set; }

        public virtual UserPosition Position { get; set; }
        public virtual Project Project { get; set; }
        public virtual User User { get; set; }
    }
}

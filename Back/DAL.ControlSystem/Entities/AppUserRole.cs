﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.ControlSystem.Entities
{
    public class AppUserRole: IdentityUserRole<int>
    {

        public User User { get; set; }
        public AppRole Role { get; set; }
    }
}

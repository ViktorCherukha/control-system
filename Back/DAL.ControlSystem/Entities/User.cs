﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace DAL.ControlSystem.Entities
{
    public class User:IdentityUser<int>
    {

        public string SurName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string Gender { get; set; }
        public DateTime LastActive { get; set; } = DateTime.Now;
        public string City { get; set; }
        public ICollection<Photo> Photos { get; set; }
        public ICollection<Job> Jobs { get; set; }
        public ICollection<AppUserRole> UserRoles { get; set; }
        public User()
        {
            Jobs = new List<Job>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.ControlSystem.Entities
{
    public class Project:BaseEntity
    {
        public override int Id { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int StateId { get; set; }
        public virtual ProjectState State { get; set; }
        
    }
}

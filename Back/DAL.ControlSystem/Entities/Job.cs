﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.ControlSystem.Entities
{
    public class Job:BaseEntity
    {
        public override int Id { get; set ; }
        public int? UserId { get; set; }
        public DateTime? DeadLine { get; set; }
        public DateTime Start { get; set; } = DateTime.Now;
        public int ProjectId { get; set; }
        public byte? JobProgress { get; set; }
        public virtual Project Project { get; set; }
        public virtual User User { get; set; }


    }
}

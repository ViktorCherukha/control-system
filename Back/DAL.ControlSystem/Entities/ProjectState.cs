﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.ControlSystem.Entities
{
    public  class ProjectState:BaseEntity
    {
        public override int Id { get; set; }
        public string State { get; set; }

        public virtual ICollection<Project> Projects { get; set; }

    }
}

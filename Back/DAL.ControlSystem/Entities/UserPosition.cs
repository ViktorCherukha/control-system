﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.ControlSystem.Entities
{
    public  class UserPosition:BaseEntity
    {
        [Key]
        public override int Id { get; set; }
        public string Position { get; set; }
    }
}

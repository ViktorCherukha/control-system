﻿
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.ControlSystem.Entities
{
    public abstract class BaseEntity
    {
        public  abstract int Id { get; set; }
    }
}

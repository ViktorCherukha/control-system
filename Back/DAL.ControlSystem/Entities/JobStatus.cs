﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.ControlSystem.Entities
{

    public class JobStatus:BaseEntity
    {
        public override int Id { get ; set ; }
        public int StatusId { get; set; }
        public int JobId { get; set; }
        public string Description { get; set; }
        public int? UserId { get; set; }
        public DateTime? LastUpdate { get; set; }

        public virtual JobStatusType Status { get; set; }
        
        public virtual Job Job { get; set; }
        public virtual User User { get; set; }
      
    }
}

﻿using DAL.ControlSystem.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.ControlSystem
{
    public class Seed
    {

        private static List<ProjectState> projectStates = new List<ProjectState>
        {
            new ProjectState
                 {
                    
                     State = "Open"
                 },
                 new ProjectState
                 {
                     
                     State = "Close"
                 }
        };
        private static List<JobStatusType> jobStatusTypes = new List<JobStatusType>
        {
             new JobStatusType
                {
                    
                    Status = "In process"
                },

                new JobStatusType
                {
                   
                    Status = "Waiting to approve"
                },

                new JobStatusType
                {
                    
                    Status = "Completed"
                }
    };
        private static List<UserPosition> userPositions = new List<UserPosition>
        {
             new UserPosition
                {
                   
                    Position = "junior",
                },
                new UserPosition
                {
                    
                    Position = "middle"
                },
                new UserPosition
                {
                   
                    Position = "senior"
                },
                new UserPosition
                {
                   
                    Position = "Lead"
                }
                };
        private static List<User> usersList = new List<User>
        {
            new User
            {
                 UserName = "Ivan",
                 SurName = "Dalnov",
                 Email = "test1@test.ua",
                 City = "Kyiv",
                 Gender = "Male",
                 DateOfBirth = DateTime.Now.AddDays(-4000),
            },
            new User
            {
                 UserName = "Kolia",
                 SurName = "Kerol",
                 Email = "test2@test.ua",
                 City = "Kyiv",
                 Gender = "Male",
                 DateOfBirth = DateTime.Now.AddDays(-4000),
            },
            new User
            {
                 UserName = "Zivan",
                 SurName = "Alber",
                 Email = "test3@test.ua",
                 City = "Kyiv",
                 Gender = "Male",
                 DateOfBirth = DateTime.Now.AddDays(-4000),
            },
            
        };

        private static List<Project> projects = new List<Project>
        {
             new Project
                {
                   
                    CloseDate = DateTime.Now.AddDays(30),
                    Name = "Car factory",
                    StartDate = DateTime.Now.AddDays(-5),
                    State = projectStates[0],

                },

                new Project
                {
                    
                    CloseDate = DateTime.Now.AddDays(10),
                    Name = "Financial company",
                    StartDate = DateTime.Now.AddDays(-2),
                    State = projectStates[0],

                },

                new Project
                {
                    
                    CloseDate = DateTime.Now.AddDays(-1),
                    Name = "Sweet company",
                    StartDate = DateTime.Now.AddDays(-12),
                    State = projectStates[1],

                }
        };

        private static List<Job> jobs = new List<Job>
        {
             new Job
                {
                    User = usersList[0],
                    DeadLine = DateTime.Now.AddDays(15),
                    Start = DateTime.Now,
                    Project = projects[0],                  
                    JobProgress = 5,

                },
                new Job
                {
                     User = usersList[1],
                    DeadLine = DateTime.Now.AddDays(10),
                    Start = DateTime.Now,
                    Project = projects[1],
                    JobProgress = 15,
                },
                new Job
                {
                    User = usersList[0],
                    DeadLine = DateTime.Now.AddDays(10),
                    Start = DateTime.Now,
                    Project = projects[1],
                    JobProgress = 25,
                }
        };

        private static List<ProjectUserPosition> projectUserPositions = new List<ProjectUserPosition>
        {
             new ProjectUserPosition
                 {
                     User = usersList[0],
                     Position = userPositions[0],
                     Project = projects[0]
                 },
                 new ProjectUserPosition
                 {
                     User = usersList[1],
                     Position = userPositions[1],
                     Project = projects[1]
                 },
                 new ProjectUserPosition
                 {
                     User = usersList[1],
                     Position = userPositions[2],
                     Project = projects[1]
                 }
        };

        private static List<JobStatus> jobStatuses = new List<JobStatus>
        {
            new JobStatus
                {

                    Job = jobs[0],
                    Status = jobStatusTypes[1],
                    User = usersList[0],
                    Description = "Fix some icon",
                    LastUpdate = DateTime.Now
                },
                new JobStatus
                {

                    Job = jobs[1],
                    Status = jobStatusTypes[0],
                    User = usersList[1],
                    Description = "Fix some button",
                    LastUpdate = DateTime.Now
                },
                new JobStatus
                {

                    Job = jobs[2],
                    Status = jobStatusTypes[0],
                    User = usersList[1],
                    Description = "Fix some label",
                    LastUpdate = DateTime.Now
                }
        };
        public static async Task SeedUsers(UserManager<User> userManager,
                RoleManager<AppRole> roleManager,ControlDbContext context)
        {
            if (await userManager.Users.AnyAsync()) return;

            var users = usersList;

            if (users.Count == 0) return;

            var roles = new List<AppRole>
            {
                new AppRole{Name = "Member"},
                new AppRole{Name = "Admin"},
                new AppRole{Name = "Moderator"},
            };

            foreach (var role in roles)
            {
                await roleManager.CreateAsync(role);
            }

            var moderator = new User
            {
                UserName = "Moderator",
                SurName = "Moderator",
                Email = "moderator@test.ua",
                City = "Kyiv",
                Gender = "Male",
                DateOfBirth = DateTime.Now.AddDays(-4000),
            };

            moderator.UserName = moderator.UserName.ToLower();
            moderator.SecurityStamp = Guid.NewGuid().ToString();
            await userManager.CreateAsync(moderator, "Password1");

            await userManager.AddToRoleAsync(moderator, "Moderator");

            foreach (var user in usersList)
            {
                user.UserName = user.UserName.ToLower();
                user.SecurityStamp = Guid.NewGuid().ToString();
                await userManager.CreateAsync(user, "Password1");

                await userManager.AddToRoleAsync(user, "Member");
            }

            context.JobStatusType.AddRange(jobStatusTypes);

            context.JobStatuses.AddRange(jobStatuses);

            context.Jobs.AddRange(jobs);

            context.ProjectStates.AddRange(projectStates);

            context.ProjectUserPositions.AddRange(projectUserPositions);

            context.Projects.AddRange(projects);

            await context.SaveChangesAsync();


            var admin = new User
            {
                UserName = "admin",
                Gender = "Male",
                SurName = "Odmen",
                City = "Kyiv",
                Email = "admin@admin.ua",
                SecurityStamp = Guid.NewGuid().ToString(),
                DateOfBirth = DateTime.Now.AddDays(-6000)

            };

            await userManager.CreateAsync(admin, "Pa$$w0rd");
            await userManager.AddToRolesAsync(admin, new[] { "Admin", "Moderator" });
          
        }
    }
}


﻿using DAL.ControlSystem.Entities;
using System.Collections.Generic;

namespace BAL.ControlSystem.Model
{
    public class UserModel
    {
        public string SurName { get; set; }
        public string UserName { get; set; }
        public string Gender { get; set; }
        public string  Token { get; set; }
        public string Email { get; set; }
        public Photo Photo { get; set; }
        public ICollection<string> Role { get; set; }

    }
}

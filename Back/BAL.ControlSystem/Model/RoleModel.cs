﻿

namespace BAL.ControlSystem.Model
{
    public class RoleModel
    {
        public int Id { get; set; }
        public string RoleType { get; set; }
    }
}

﻿using System;

namespace BAL.ControlSystem.Model
{
    public class ProjectModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int StateId { get; set; }
        public virtual ProjectStateModel State { get; set; }
      
    }
}

﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BAL.ControlSystem.Model
{
    public class RegisterModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 4)]
        public string Password { get; set; }
        //[Required]
        public DateTime DateOfBirthday { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Gender { get; set; }
        [EmailAddress]
        public string Email { get; set; }
    }
}

﻿using System;

namespace BAL.ControlSystem.Model
{
    public class JobModel
    {
        public  int Id { get; set; }
        public int UserId { get; set; }
        public DateTime? DeadLine { get; set; }
        public DateTime? Start { get; set; }
        public int ProjectId { get; set; }
        public byte? Progress { get; set; }
        public  ProjectModel Project { get; set; }
        public  UserModel User { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BAL.ControlSystem.Model;
using DAL.ControlSystem.Entities;

namespace BAL.ControlSystem
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserModel>().ReverseMap();
           
            CreateMap<User, LoginModel>().ReverseMap();
            CreateMap<Project, ProjectModel>().ReverseMap();
            CreateMap<Job, JobModel>().ReverseMap();
            CreateMap<RegisterModel, User>().ReverseMap();
            CreateMap<JobModel, Job>().ReverseMap();
            CreateMap<ProjectState, ProjectStateModel>().ReverseMap();
        }
    }
}

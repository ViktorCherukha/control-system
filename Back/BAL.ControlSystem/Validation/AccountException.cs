﻿using System;
namespace BAL.ControlSystem.Validation
{
    public class AccountException:Exception
    {
        public int? ErrorCode { get; set; }
        public string  Error { get; set; }
        public string Description { get; set; }

        public AccountException(string error,int errorcode =0,string description = null)
        {
            ErrorCode = errorcode;
            Error = error;
            Description = description;
        }
    }
}

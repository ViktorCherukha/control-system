﻿using BAL.ControlSystem.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BAL.ControlSystem.Interfaces
{
    public interface IJobService:ICrud<JobModel>
    {
        Task<IEnumerable<JobModel>> GetJobByUserEmail(string email);
    }
}

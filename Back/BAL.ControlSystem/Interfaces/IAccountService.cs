﻿using BAL.ControlSystem.Model;
using System.Threading.Tasks;

namespace BAL.ControlSystem.Interfaces
{
    public interface IAccountService
    {
        Task<UserModel> RegisterUser(RegisterModel model);
        Task<UserModel> Login(LoginModel model);
    }
}

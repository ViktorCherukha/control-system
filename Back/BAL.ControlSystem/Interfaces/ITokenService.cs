﻿using DAL.ControlSystem.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BAL.ControlSystem.Interfaces
{
    public interface ITokenService
    {
        Task<string> CreateToken(User user);
    }
}

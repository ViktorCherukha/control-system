﻿using AutoMapper;
using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BAL.ControlSystem.Services
{
    public class ProjectService : IProjectService
    {

        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ProjectService(IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task AddAsync(ProjectModel model)
        {
            var project = _mapper.Map<ProjectModel, Project>(model);
            await _unitOfWork.ProjectRepository.AddAsync(project);
            await _unitOfWork.SaveAsync();

        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.ProjectRepository.DeleteById(id);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<ProjectModel> GetAll()
        {
            var project =  _unitOfWork.ProjectRepository.FindAllWithDetails();
            return _mapper.Map<IEnumerable<Project>, IEnumerable<ProjectModel>>(project);
        }

        public async Task<ProjectModel> GetByIdAsync(int id)
        {
            var project = await _unitOfWork.ProjectRepository.GetByIdWithDetails(id);
            return _mapper.Map<Project, ProjectModel>(project);
        }

        public async Task UpdateAsync(ProjectModel model)
        {
            var project = _mapper.Map<ProjectModel, Project>(model);
            _unitOfWork.ProjectRepository.Update(project);
            await _unitOfWork.SaveAsync();
        }
    }
}

﻿using AutoMapper;
using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using DAL.ControlSystem.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BAL.ControlSystem.Services
{
    public class AdminService:IAdminService
    {
        private readonly UserManager<User> _usermanager;
        private readonly IMapper _mapper;

        public AdminService(UserManager<User> userManager,IMapper mapper)
        {
            _usermanager = userManager;
            _mapper = mapper;
        }

        public async Task<IEnumerable<UserModel>> GetUsersWithRoles()
        {
            var users = _mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>
                (await _usermanager.Users
                .Include(r => r.UserRoles)
                .ThenInclude(r => r.Role)
                .ToListAsync());

            return users;
        }
    }
}

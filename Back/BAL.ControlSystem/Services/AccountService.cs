﻿using AutoMapper;
using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using BAL.ControlSystem.Validation;
using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using Microsoft.AspNetCore.Identity;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace BAL.ControlSystem.Services
{
    public class AccountService : IAccountService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly ITokenService _token;
        private StringBuilder _error;


        public AccountService(IMapper mapper,IUnitOfWork unitOfWork,ITokenService token,UserManager<User> userManager
            ,SignInManager<User> signInManager)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _signInManager = signInManager;
            _token = token;
        }

        public async Task<UserModel> RegisterUser(RegisterModel model)
        {
            if (await _unitOfWork.UserRepository.IsUserExist(model.UserName))
                throw new AccountException("User name is taken");

            var user = _mapper.Map<RegisterModel, User>(model);

            user.UserName = user.UserName.ToLower();

            var result = await _userManager.CreateAsync(user, model.Password);

            _error = new StringBuilder();

            foreach (var item in result.Errors)
            {
                _error.Append(item);
            }

            if (!result.Succeeded)
                throw new AccountException(_error.ToString());

            var roleresult = await _userManager.AddToRoleAsync(user, "Member");

            foreach (var item in result.Errors)
            {
                _error.Append(item);
            }


            if (!roleresult.Succeeded)
                throw new AccountException(_error.ToString());

            return new UserModel
            {
                UserName = model.UserName,
                Gender = model.Gender,
                Token = await _token.CreateToken(user),
                Role = await _userManager.GetRolesAsync(user)
            };

        }
        public async Task<UserModel> Login(LoginModel model)
        {

            var user = _userManager.Users.SingleOrDefault(e => e.UserName == model.UserName);

            if (user == null)
                throw new AccountException("User not register");

            var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);

            if (!result.Succeeded)
                throw new AccountException("Not authorize");

            return new UserModel
            {
                UserName = user.UserName,
                SurName = user.SurName,
                Token = await _token.CreateToken(user),
        
                Role = await _userManager.GetRolesAsync(user)
            };

        }
    }
}

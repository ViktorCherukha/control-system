﻿using AutoMapper;
using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace BAL.ControlSystem.Services
{
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public UserService(IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task AddAsync(UserModel model)
        {
            var user = _mapper.Map<UserModel, User>(model);
            await _unitOfWork.UserRepository.AddAsync(user);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.UserRepository.DeleteById(id);
            await _unitOfWork.SaveAsync();
        }

        public IEnumerable<UserModel> GetAll()
        {
            var users = _unitOfWork.UserRepository.FindAllwithDetails();

            return _mapper.Map<IEnumerable<User>, IEnumerable<UserModel>>(users);

        }

        public async Task<UserModel> GetByIdAsync(int id)
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(id);

            return  _mapper.Map<User, UserModel>(user);
        }

        public async Task UpdateAsync(UserModel model)
        {
            var user = _mapper.Map<UserModel, User>(model);
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.SaveAsync();
        }
    }
}

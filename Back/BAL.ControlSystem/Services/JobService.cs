﻿using AutoMapper;
using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using DAL.ControlSystem.Entities;
using DAL.ControlSystem.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BAL.ControlSystem.Services
{
    public class JobService : IJobService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public JobService(IMapper mapper,IUnitOfWork unitOfWork)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task AddAsync(JobModel model)
        {
            var job = _mapper.Map<JobModel, Job>(model);
            await _unitOfWork.JobRepository.AddAsync(job);
            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteByIdAsync(int id)
        {
            await _unitOfWork.JobRepository.DeleteById(id);
            await _unitOfWork.SaveAsync();
        }
        public async Task<IEnumerable<JobModel>> GetJobByUserEmail(string email)
        {
            var jobs = await  _unitOfWork.JobRepository.GetByUserEmailWithDetails(email);
            return _mapper.Map<IEnumerable<JobModel>>(jobs);
        }
        public  IEnumerable<JobModel> GetAll()
        {
            var jobs = _unitOfWork.JobRepository.FindAllWithDetails();
            return _mapper.Map<IEnumerable<Job>, IEnumerable<JobModel>>(jobs);

        }

        public async Task<JobModel> GetByIdAsync(int id)
        {
           
            var job =  await _unitOfWork.JobRepository.GetByIdAsync(id);
            return _mapper.Map<Job, JobModel>(job);
        }

        public async Task UpdateAsync(JobModel model)
        {
            var job = _mapper.Map<JobModel, Job>(model);
            _unitOfWork.JobRepository.Update(job);
            await _unitOfWork.SaveAsync();

        }
    }
}

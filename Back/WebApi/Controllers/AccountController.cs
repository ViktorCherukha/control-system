﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using BAL.ControlSystem.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
   
    public class AccountController : BaseApiController
    {
        private readonly IAccountService _accountservice;

        public AccountController(IAccountService accountService)
        {
            _accountservice = accountService;
        }

        [HttpPost("register")]
        public async Task<ActionResult<UserModel>> Register(RegisterModel model)
        {
            try
            {
                var user = await _accountservice.RegisterUser(model);
                return Ok(user);
            }
            catch(AccountException ex) 
            {

                return BadRequest(ex.Error);
            }
        }

        [HttpPost("login")]

        public async Task<ActionResult<UserModel>> Login(LoginModel model)
        {
            try
            {
                var user = await _accountservice.Login(model);
                return Ok(user);
            }
            catch (AccountException ex)
            {

                return Unauthorized(ex.Error);
            }
        }
    }
}

﻿
using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{

    public class UsersController:BaseApiController
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userservice)
        {
            _userService = userservice;
        }


        [Authorize(Policy = "ModerateRole")]
        [HttpGet]
        public ActionResult<IEnumerable<UserModel>> GetUsers()
        {
            var users = _userService.GetAll();

            return Ok(users);
        }
        //[Authorize]
        [HttpGet("{id}")]
        public async Task<UserModel> GetUser(int id)
        {

            var user = await _userService.GetByIdAsync(id);

            return user;

        }
    }
}

﻿using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    public class ProjectController :BaseApiController
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult GetProjects()
        {
            var projects = _projectService.GetAll();
            return Ok(projects);
        }
        [HttpGet("{id}")]
        public async Task<ActionResult> GetProjectById(int id)
        {
            var project = await _projectService.GetByIdAsync(id);
            return Ok(project);
        }

        [HttpPost]
        public async Task<ActionResult> AddProject(ProjectModel model)
        {
            await _projectService.AddAsync(model);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult>DeletebyId(int id)
        {
            await _projectService.DeleteByIdAsync(id);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> Update(ProjectModel model)
        {
            await _projectService.UpdateAsync(model);
            return Ok();
        }
    }
}

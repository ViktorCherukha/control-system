﻿using DAL.ControlSystem.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{
    public class ErrorsController :BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        public ErrorsController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [Authorize]
        [HttpGet("auth")]
        public ActionResult<string> GetSecret()
        {
            return Ok("secret text");
        }

        [HttpGet("not-found")]
        public async Task<ActionResult> GetNotFound()
        {
            var user = await _unitOfWork.UserRepository.GetByIdAsync(-1);

            if (user == null) return NotFound();

            return Ok(user);
        }

        [HttpGet("server-error")]
        public  ActionResult<string> GetServerError()
        {

            var user = _unitOfWork.UserRepository.FindAll().FirstOrDefault(e => e.Id == -1);

            var result = user.ToString();

            return result;

        }

        [HttpGet("bad-request")]
        public ActionResult<string> GetBadRequest()
        {
            return BadRequest("Bad request from server");
        }
    }
}

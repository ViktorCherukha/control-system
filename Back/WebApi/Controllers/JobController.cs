﻿using BAL.ControlSystem.Interfaces;
using BAL.ControlSystem.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebApi.Controllers
{

    public class JobController : BaseApiController
    {

        private readonly IJobService _jobService;
        public JobController(IJobService userservice)
        {
            _jobService = userservice;
        }

        [HttpGet]
        public ActionResult<IEnumerable<JobModel>> GetJobs()
        {
            return Ok(_jobService.GetAll());
        }

        [HttpGet("user/{email}")]
        public async Task<ActionResult> GetJobsById(string email)
        {
            return Ok( await _jobService.GetJobByUserEmail(email));
        }

        [HttpPost]
        public async Task<ActionResult> AddJobsAsync(JobModel model)
        {
            await _jobService.AddAsync(model);
            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteJobAsync(int id)
        {
            await _jobService.DeleteByIdAsync(id);
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> UpdateJobAsync(JobModel model)
        {
            await _jobService.UpdateAsync(model);
            return Ok();
        }
    }
}

﻿
using BAL.ControlSystem.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Controllers
{

    public class AdminController : BaseApiController
    {
        private readonly IAdminService _adminService;
        public AdminController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        [Authorize(Policy = "AdminRole")]
        [HttpGet("users-with-roles")]
        public async Task<ActionResult> GetUsersWithRoles()
        {
            return Ok("Only for admins");
        }

        [Authorize(Policy ="AdminRole")]
        [HttpPost("edit-roles/{username}")]
        public async Task<ActionResult> EditRoles(string name, [FromQuery] string roles)
        {
            var selectedRoles = roles.Split(",").ToArray();

            //find user by user manager

            //selected roles - find user roles user manager

            // user managet add to roles async  (user,selected roles, except (user roles))

            //check result 

            // remove user from selected roles
            return Ok();
        }
    }
}
